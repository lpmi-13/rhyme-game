# Rhyme Match Game

based off the work of Seif Ghezala in [this article](https://hackernoon.com/how-to-create-a-pwa-game-using-preact-in-5-steps-tutorial-c8b177037c80).

This will eventually be an offline first way for EFL students to practice identifying words that rhyme.
